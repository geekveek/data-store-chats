const ADD_TODO = Symbol('add todo');
const REMOVE_TODO = Symbol('remove todo');
const EDIT_TODO = Symbol('edit todo');

const todoDM = (function() {
  const {AlDataStore} = require('./data-store.js');
  return new AlDataStore({
    registeredActions: {
      [ADD_TODO]: addTodo,
      [REMOVE_TODO]: removeTodo,
      [EDIT_TODO]: editTodo,
    },
    initialState: {title: {id: Symbol(), description: 'blah'},todos: []},
  });

  // -- ACTIONS (Functional, easy to test) --
  // Add todo
  function addTodo(state, {title: todoTitle}) {
    const {todos} = state;
    const newTodo = {id: Symbol(), title: todoTitle};
    return Object.assign({}, state, {todos: [...todos, newTodo]});
  }

  // Remove todo
  function removeTodo(state, {id}) {
    const {todos} = state;
    return Object.assign({}, state, {
      todos: todos.filter(todo => todo.id !== id),
    });
  }

  // Edit todo
  function editTodo(state, {id, title}) {
    const {todos} = state;
    return Object.assign({}, state, {
      todos: todos.map(
        todo => (todo.id === id ? Object.assign({}, todo, {title}) : todo),
      ),
    });
  }

  // Example tests
  /**
   * expect(addTodo({todos: []}, {title: 'hello'}))
   * 	.toBe({todos: [jasmine.objectContains({title: 'hello'})]});
   **/
})();

//In the different components
let firstTodo;
todoDM.subscribe(todoState => {
  firstTodo = todoState.todos[0]; //For demo
  console.log(todoState);
});

// Something somewhere added a todo (where this came from does not matter)
async function main() {
  await todoDM.dispatch({type: ADD_TODO, payload: {title: 'Foo'}});

  await todoDM.dispatch({
    type: EDIT_TODO,
    payload: {id: firstTodo.id, title: 'Bar'},
  });

  await todoDM.dispatch({
    type: REMOVE_TODO,
    payload: {id: firstTodo.id},
  });
}

main();
