// Adapted from AffinityLive repo frontend/lib/utility/subscription-manager/subscription-manager.class.js
/**
 * @class AlSubscriptionManager
 *
 * @description The main use of this class is to manage subscriptions.
 * Subscriptions are callbacks that will be invoked when AlSubscriptionManager.runSubscription({key}) is invoked.
 *
 * This uses publish-subscribe pattern to manage when subscriptions are ran
 * This implementation of the subscribe uses callbacks rather than objects as subscribers
 * @see https://en.wikipedia.org/wiki/Publish%E2%80%93subscribe_pattern
 */

class AlSubscriptionManager {
	/**
	 * this._subscriptions looks like
	 * {
	 * 	'KEY': [<fn>]
	 * }
	 */
	constructor() {
		this._subscriptions = {};
	}

	/**
	 * Adds the subscription to the subscription manager.
	 * @param {string} options.key      the key to store this subscription under.
	 * @param {function} options.callback the callback to call when subscription is ran. This callback expects no parameter.
	 * @return {function} unsubscribe function, when invoked, will unsubscribe the callback.
	 */
	subscribe({ key, callback }) {
		if (!key) {
			throw new Error('key is required to add subscription');
		}
		if (!callback) {
			throw new Error('callback is required to add subscription');
		}
		if (typeof callback !== 'function') {
			throw new Error('callback must be a function to add subscription');
		}
		if (!this._subscriptions[key]) {
			this._subscriptions[key] = [];
		}
		this._subscriptions[key].push(callback);
		return () => {
			this._subscriptions[key] = this._subscriptions[key].filter(storedCallback => storedCallback !== callback);
		};
	}

	/**
	 * Runs callbacks for subscription
	 * @param  {string} key of the subscription
	 * @param {Object} [parameters] additional parameters to call back with
	 */
	publish({ key, parameters }) {
		if (!key) {
			throw new Error('key is required to run subscription');
		}

		const subscriptions = this._subscriptions[key] || [];
		subscriptions.forEach(callback => callback(parameters));
	}

	/**
	 * Gets the number of subscription for a given key
	 * @param  {string} key of the subscription
	 * @return {int}     number of subscriptions for the given key
	 */
	numberOfSubscriptionsForKey(key) {
		if (!this._subscriptions[key]) {
			return 0;
		}
		return this._subscriptions[key].length;
	}
}

module.exports.AlSubscriptionManager = AlSubscriptionManager;
